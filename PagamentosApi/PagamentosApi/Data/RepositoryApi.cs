﻿namespace PagamentosApi.Data;

public abstract class RepositoryApi
{
    protected readonly ApiContext _context;
    public RepositoryApi(ApiContext context)
    {
        _context = context;
    }

    public async Task Save()
    {
        await _context.SaveChangesAsync();
    }
}