﻿namespace PagamentosApi.Controllers;

[Produces("application/json")]
[ApiController]
[Route("[controller]")]
public class VendaController : ControllerBase
{
    private readonly VendaService _service;
    public VendaController(VendaService service)
    {
        _service = service;
    }

    [ProducesResponseType(typeof(VendaResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status422UnprocessableEntity)]
    [HttpPost]
    public async Task<IActionResult> Create(VendaRequest request)
    {
        var venda = await _service.Create(request);
        return CreatedAtAction(
          actionName: nameof(GetById),
          routeValues: new { id = venda.Id },
          value: venda
        );
    }

    [ProducesResponseType(typeof(VendaResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
    [HttpGet("{id}")]
    public async Task<IActionResult> GetById(uint id)
    {
        return Ok(value: await _service.GetById(id));
    }

    [ProducesResponseType(typeof(VendaResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
    [HttpPatch("{id}")]
    public async Task<IActionResult> UpdateStatus(uint id, [FromBody] StatusRequest request)
    {
        return Ok(value: await _service.UpdateStatus(id, request));
    }
}
