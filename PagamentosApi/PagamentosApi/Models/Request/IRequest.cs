namespace PagamentosApi.Models.Request;

public interface IRequest<Record>
{
  Record ToRecord();
}