namespace PagamentosApi.Models.Request;

public class StatusRequest
{
  public EStatus Status { get; set; }
}