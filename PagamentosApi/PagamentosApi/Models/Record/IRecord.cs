namespace PagamentosApi.Models.Record;

public interface IRecord<Response>
{
  Response ToResponse();
}